package br.com.futureofteaching;

import javax.persistence.EntityManagerFactory;
import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.web.filter.CharacterEncodingFilter;

public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(WebServiceFutureOfTeachingApplication.class);
	}
	
	/*
	 * Serve para achar as configurações do sistema para os Controllers
	 */
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[] { WebServiceFutureOfTeachingApplication.class };
	}

	/*
	 * Serve para achar o caminho dos Controllers definido pelo sistema
	 */
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

	/*
	 * Irá fazer o tratamento de todos os filtros de linguagem para UTF-8
	 */
	protected Filter[] getServletFilters() {
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding("UTF-8");
		characterEncodingFilter.setForceEncoding(true);

		return new Filter[] { characterEncodingFilter };
	}

	/*
	 * Serve para configurar o recebimento de múltiplos arquivos Ele configura de
	 * forma automática o tamanho do arquivo
	 */
	protected void customizeRegistration(Dynamic registration) {
		registration.setMultipartConfig(new MultipartConfigElement(""));
	}
	
	@Autowired
	private EntityManagerFactory entityManagerFactory;

	@Bean
	public PlatformTransactionManager jpaTransactionManager() {
	    JpaTransactionManager jpaTransactionManager = new JpaTransactionManager(entityManagerFactory);
	    return jpaTransactionManager;
	}

}
