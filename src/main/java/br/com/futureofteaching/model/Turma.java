package br.com.futureofteaching.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "tb_turma")
public class Turma implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long codigoTurma;

	@NotNull(message = "A descrição é obrigatória")
	private String descricao;

	@NotNull(message = "O turno é obrigatório")
	private String turno;

	@NotNull(message = "O curso é obrigatório")
	private String curso;

	@NotNull(message = "A série ou período é obrigatório")
	private String seriePeriodo;

	/*
	 * FK Curso código curso?
	 */
	
	public Long getCodigoTurma() {
		return codigoTurma;
	}

	public void setCodigoTurma(Long codigoTurma) {
		this.codigoTurma = codigoTurma;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getTurno() {
		return turno;
	}

	public void setTurno(String turno) {
		this.turno = turno;
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

	public String getSeriePeriodo() {
		return seriePeriodo;
	}

	public void setSeriePeriodo(String seriePeriodo) {
		this.seriePeriodo = seriePeriodo;
	}

}
