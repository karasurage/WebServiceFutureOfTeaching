package br.com.futureofteaching.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "tb_curso")
public class Curso implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long codigoCurso;

	@NotNull(message = "O nível de ensino é obrigatória")
	private String nivelEnsino;

	@NotNull(message = "A disciplina é obrigatória")
	private Disciplina disciplina;

//	@OneToMany
	/*
	 * Fazer um FK de código IE
	 */
	private IE codigoIE;

	public Long getCodigoCurso() {
		return codigoCurso;
	}

	public void setCodigoCurso(Long codigoCurso) {
		this.codigoCurso = codigoCurso;
	}

	public String getNivelEnsino() {
		return nivelEnsino;
	}

	public void setNivelEnsino(String nivelEnsino) {
		this.nivelEnsino = nivelEnsino;
	}

	public Disciplina getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}

	public IE getCodigoIE() {
		return codigoIE;
	}

	public void setCodigoIE(IE codigoIE) {
		this.codigoIE = codigoIE;
	}

}
