package br.com.futureofteaching.model;

public enum Sexo {

	MASCULINO("Masculino"),
	FEMININO("Feminino"),
	OUTROS("Outros"),
	NAO_DECLARADO("Não Declarado");

	private String descricao;

	Sexo(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
