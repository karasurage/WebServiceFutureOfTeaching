package br.com.futureofteaching.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "tb_contato")
public class Contato implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long codigoContato;

	@NotNull(message = "O código da área é obrigatória")
	private String codigoArea;

	private String foneResidencial;

	@NotNull(message = "O telefone celular é obrigatório")
	private String celular;

	@NotNull(message = "O número do whatsapp é obrigatório")
	private String whatsapp;

	@NotBlank(message = "O e-mail é obrigatório")
	private String email;

	/*
	 * FK
	 */
//	@OneToMany
//	private IE codigoIE;
	
	/*
	 * FK pessoa_usuario --> codigoPessoa?
	 */
	
	/*
	 * FK pessoa_usuario --codigoUsuario?
	 */

	/* GETTERS AND SETTERS */

	public String getCodigoArea() {
		return codigoArea;
	}

	public void setCodigoArea(String codigoArea) {
		this.codigoArea = codigoArea;
	}

	public String getFoneResidencial() {
		return foneResidencial;
	}

	public void setFoneResidencial(String foneResidencial) {
		this.foneResidencial = foneResidencial;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
