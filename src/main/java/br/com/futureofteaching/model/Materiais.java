package br.com.futureofteaching.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "tb_materiais")
public class Materiais implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long codigoMaterial;

	@NotNull(message = "O tipo é obrigatório")
	private String tipo;

	@NotBlank
	private Boolean exclusivoProfessor;

	@NotNull(message = "A descrição é obrigatória")
	@Max(value = 500, message = "A descrição deve ter até 500 caracteres")
	private String descricao;

//	@ManyToMany
	/*
	 *  Fazer um JoinColumn para concatenar a tabela "tb_tem"
	 */
	private List<Disciplina> disciplina;

	public Long getCodigoMaterial() {
		return codigoMaterial;
	}

	public void setCodigoMaterial(Long codigoMaterial) {
		this.codigoMaterial = codigoMaterial;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Boolean getExclusivoProfessor() {
		return exclusivoProfessor;
	}

	public void setExclusivoProfessor(Boolean exclusivoProfessor) {
		this.exclusivoProfessor = exclusivoProfessor;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Disciplina> getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(List<Disciplina> disciplina) {
		this.disciplina = disciplina;
	}

}
