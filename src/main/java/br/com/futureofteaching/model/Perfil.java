package br.com.futureofteaching.model;

public enum Perfil {

	PROFESSOR("Professor"),
	ALUNO("Aluno"),
	RESPONSAVEL("Responsável"),
	GESTÃO("Gestão");

	private String descricao;

	Perfil(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
