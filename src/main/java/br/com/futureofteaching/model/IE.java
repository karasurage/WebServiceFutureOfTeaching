package br.com.futureofteaching.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "tb_ie")
public class IE implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long codigoIE;

	@NotNull(message = "O CNPJ é obrigatório")
	private String cnpj;

	@NotNull(message = "O nome fantasia é obrigatório")
	private String nomeFantasia;

	@NotNull(message = "A razão social é obrigatória")
	private String razaoSocial;

	@NotBlank
	private Endereco endereco;

	/* NÃO ENTENDI ISSO???? */
	@NotNull(message = "Os dados são obrigatórios")
	private String dados;

	/*
	 * FK endereço --> código endereço
	 */

	public Long getCodigoIE() {
		return codigoIE;
	}

	public void setCodigoIE(Long codigoIE) {
		this.codigoIE = codigoIE;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getDados() {
		return dados;
	}

	public void setDados(String dados) {
		this.dados = dados;
	}

}
