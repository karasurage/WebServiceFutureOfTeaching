package br.com.futureofteaching.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.futureofteaching.model.Usuario;

@Repository
public interface UsuariosRepository extends JpaRepository<Usuario, Long> {

//	public Optional<Usuario> findByEmail(String email);
	Usuario findById(long codigo);

}
