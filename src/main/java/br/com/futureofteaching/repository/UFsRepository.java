package br.com.futureofteaching.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.futureofteaching.model.UF;

@Repository
public interface UFsRepository extends JpaRepository<UF, Long> {

}
