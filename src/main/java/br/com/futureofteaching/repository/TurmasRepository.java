package br.com.futureofteaching.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.futureofteaching.model.Turma;

@Repository
public interface TurmasRepository extends JpaRepository<Turma, Long> {

}
