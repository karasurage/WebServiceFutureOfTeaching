package br.com.futureofteaching.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.futureofteaching.model.Endereco;

@Repository
public interface EnderecosRepository extends JpaRepository<Endereco, Long> {

}
