package br.com.futureofteaching.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.futureofteaching.model.Contato;

@Repository
public interface ContatosRepository extends JpaRepository<Contato, Long> {

}
