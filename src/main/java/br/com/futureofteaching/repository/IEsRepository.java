package br.com.futureofteaching.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.futureofteaching.model.IE;

@Repository
public interface IEsRepository extends JpaRepository<IE, Long> {

}
