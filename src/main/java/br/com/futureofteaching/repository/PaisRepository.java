package br.com.futureofteaching.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.futureofteaching.model.Pais;

@Repository
public interface PaisRepository extends JpaRepository<Pais, Long> {

}
