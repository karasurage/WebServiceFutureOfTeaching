package br.com.futureofteaching.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.futureofteaching.model.Curso;

@Repository
public interface CursosRepository extends JpaRepository<Curso, Long> {

}
