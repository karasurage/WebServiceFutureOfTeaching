package br.com.futureofteaching.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.futureofteaching.model.Materiais;

@Repository
public interface MateriaisRepository extends JpaRepository<Materiais, Long> {

}
