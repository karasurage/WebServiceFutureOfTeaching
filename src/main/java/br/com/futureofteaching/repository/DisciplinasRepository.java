package br.com.futureofteaching.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.futureofteaching.model.Disciplina;

@Repository
public interface DisciplinasRepository extends JpaRepository<Disciplina, Long> {

}
