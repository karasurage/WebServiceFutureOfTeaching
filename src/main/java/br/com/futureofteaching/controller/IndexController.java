package br.com.futureofteaching.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/")
public class IndexController {

	@RequestMapping("/home")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("Index");
		return mv;
	}
}
