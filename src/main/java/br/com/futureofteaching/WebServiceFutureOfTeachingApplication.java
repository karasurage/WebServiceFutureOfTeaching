package br.com.futureofteaching;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "br.com.futureofteaching.controller" )
public class WebServiceFutureOfTeachingApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebServiceFutureOfTeachingApplication.class, args);
	}

}
