# Future of Teaching - FoT

## Descrição do Projeto

<p align="justify">O projeto tem a iniciativa de mostrar ferramentas na plataforma que servirá de apoio para a área educacional, visando atender como público-alvo os educadores, responsáveis, alunos e gestores das instituições de ensino.
A plataforma disponibilizará de meios e ferramentas para darem apoio ao público-alvo proposto, tendo em vista o uso de formas digitais no contexto educacional de ensino.
<p align="justify">Haverá instruções aos educadores de como utilizar ferramentas digitais em sala de aula, através de materiais de apoio que contemplaram vídeo aulas, tutoriais, indicações de aplicativos e seu respectivo uso, além de algumas ferramentas extras como controle de frequência, cadastro de turmas, alunos e pais para recebimento de notificações.
Fora as informações supracitadas, teremos também o uso de ferramentas de acessibilidade em nossa plataforma, visando a inclusão de pessoas com algum tipo de inaptidão ou deficiência.</p>

<ul><h2>Integrantes do Projeto</h2></2>
<li>Ewerton Ricardo</li>
<li>Nicholas Mateus</li>
<li>Randerson Thallys</li>
<li>Stewart Almeida</li>
</ul>

Os integrantes supracitados são da Universidade São Miguel do curso superior de Tecnólogo em Sistemas para Internet, cursando atualmente o 4º Período.
